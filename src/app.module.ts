import { Inject, DynamicModule, Module } from '@nestjs/common'

@Module({})
class Foo {
  static register(): Promise<DynamicModule> {
    return Promise.resolve({
      module: Foo,
      providers: [{
        provide: 'foo',
        useValue: 123,
      }],
      exports: ['foo']
    })
  }
}

const whenModule = Foo.register()

@Module({
  imports: [
    whenModule,
  ],
  exports: [
    // Foo,
    whenModule,
  ]
})
class Bar {}

@Module({
  imports: [Bar]
})
export class AppModule {
  constructor(@Inject('foo') readonly foo: any) {}
}
